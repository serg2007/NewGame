﻿using UnityEngine;
using System.Collections;

public class MainSceneScript : MonoBehaviour {
	public GameObject[] prototypes = new GameObject[4];

	public float speed = 10f;
	private float addObjectTime = 1f;
	private int createdObjectCount = 0;
	// Use this for initialization
	void Start () {
//		for (int i = 0; i <= 100; i++) {
//						CreateObject ();
//				}
		StartInitialisation ();
	}
	
	// Update is called once per frame
	void Update () {
		addObjectTime -= Time.deltaTime;

		if (addObjectTime < 0) {
			CreateObject (Random.Range (0, 6) - 3, 10);
			addObjectTime = 1f;
		}
	}

	void StartInitialisation()
	{
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 15; j++) {
				CreateObject (((float)i) / 2 - 3f, j);

			}	
		}
	}

	void CreateObject (float x, float y) {
		createdObjectCount++;
		Vector3 clonePosition = transform.position;
		clonePosition.z = 0;
		clonePosition.x = x;
		clonePosition.y = y;
		Quaternion cloneRotation = transform.rotation;
		cloneRotation.z = Random.Range (0.0f, 1.0f);

		Instantiate (prototypes[Random.Range(0, prototypes.Length)], clonePosition, cloneRotation);
	}

	public void OnButtonClick()
	{
		Application.LoadLevel (Application.loadedLevel);
	}

}
