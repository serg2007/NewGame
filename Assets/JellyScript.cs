﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JellyScript : MonoBehaviour {
	public List<GameObject> contactObjects = new List<GameObject>();
	public bool marked = false;

	List<GameObject> listForRemove = new List<GameObject>();

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		
		if(hit.collider != null)
		{
			if(Input.GetMouseButtonDown(0))
			{
				if(!hit.collider.gameObject.tag.Equals("Bound")){
					Debug.Log ("Target Position: " + hit.collider.gameObject.transform.position);
					if(gameObject ==  hit.collider.gameObject){
						if (gameObject) {
							if(contactObjects.Count >= 1) {//TODO add freaze for object
								Destroy(gameObject);
//								this.marked = true;
								RemoveContactObjects(this);
							}
						}
					}
					
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collider) 
	{
		// Debug-draw all contact points and normals
		if(collider.gameObject.tag.Equals(gameObject.tag))
			contactObjects.Add (collider.gameObject);
		//Collision detected!
	}

	void OnTriggerExit2D(Collider2D collider) 
	{
		// Debug-draw all contact points and normals
		if(!collider.gameObject.tag.Equals("Bound"))
			contactObjects.Remove (collider.gameObject);

		//Collision detected!
	}

	public void RemoveContactObjects(JellyScript previosNode = null)
	{
//		
//		

		foreach (var contactObject in contactObjects) 
		{
			if(contactObject != null){
				JellyScript jellyScript = contactObject.GetComponent<JellyScript>();
				if (!jellyScript.marked) {
//					jellyScript.contactObjects.Remove(previosNode.gameObject);
//					Destroy (previosNode);
					listForRemove.Add (jellyScript.gameObject);
					jellyScript.marked = true;
					jellyScript.GetComponent<SpriteRenderer>().color = Color.gray;
					jellyScript.RemoveContactObjects(jellyScript);

				}

			}
		}

		foreach (var removeNode in listForRemove)
		{
			Destroy(removeNode);
		}
//		contactObjects.RemoveRange(0, contactObjects.Count - 1);

	}

}
